<?php

namespace App\Http\Controllers;

use App\Employe;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class EmployesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $files = $request->file('file');
        
       
        
        
    	if(!empty($files)){
            $filename = Str::random(20).".".$files->getClientOriginalExtension();
            $storageDestinationPath=storage_path("app/public/employes/February2020");
            $destinationPath="employes/February2020";
            
            $fileJson = array(
                array(
                    "download_link" => $destinationPath.'/'.$filename,
                    "original_name" => $files->getClientOriginalName()
                )
            );

            $fileJson = json_encode($fileJson);
            $files->move($storageDestinationPath, $filename);
            // move_uploaded_file ($filename , $storageDestinationPath );
            Employe::create([
                "name" => $request['name'],
                "cv" => $fileJson,
                "email" => $request['email'],
                "message" => $request['message'],
            ]);
            return redirect()->route('success');
            
            

        }

    	
       
        
      
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
